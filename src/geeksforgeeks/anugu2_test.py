'''
Created on Feb 26, 2021

@author: Ari Anugu
'''
from geeksforgeeks.algorithm import lcs
print("Length of LCS #1 is ", lcs("AGGTAB", "GXTXAYB"))
print("Length of LCS #2 is ", lcs("ABCDGH", "AEDFHR"))
print("Length of LCS #3 is ", lcs("AEFCDB", "AGCDBR"))

