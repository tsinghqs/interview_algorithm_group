'''
Created on Feb 26, 2021

@author: tsinghqs
'''
import unittest
from geeksforgeeks.algorithm import lcs

class lcsTest(unittest.TestCase):
    def test(self):
        #declare needed variables for testing
        a = abcdefg
        b = abedfeg

        c = longestcommongsubstring
        d = longerthatthis

        #assert statements
        self.assertEqual(lcs(a, b),2)
        self.assertEqual(lcs(c,d),5)
       

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
